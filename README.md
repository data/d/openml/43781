# OpenML dataset: Movinga-Best-Cities-for-Families-2019

https://www.openml.org/d/43781

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data was scraped from Movinga (https://www.movinga.com/), the global moving company and determines the top cities around the world that offer the best conditions to raise a family for 2019. The dataset was extended to add geographical location in terms of the latitude and longitude of the city. The original data can be found here. From their website:
"We began the study by selecting 150 international cities that have a reputation as attractive locations for raising a family. We then split the study into numerous factors across three categories which indicate how family-friendly a location is. This included essentials that affect city livability like housing, education, employment rates, and general affordability, as well as family legislation such as the amount of paid parental leave and whether a city is inclusive for same-sex parents. 

It was important to include the opinions of the families who experience these cities themselves, so we commissioned two surveys of parents in each location to gain a measurement of public sentiment towards them. The first survey asked parents to indicate how they felt about their childrens safety in the community, and the second if they believed that their city was a good place for families in general. Last but not least, we looked at the attractiveness and breadth of a citys family-oriented leisure activities. 

The final index combines all of these factors to determine the top cities around the world that offer the best conditions to raise a family."
Content
This data was scraped from Movinga (https://www.movinga.com/), the global moving company. The original data can be found here. The data includes 18 columns:

City
Country
Housing Affordability
Living Costs by Income
Unemployment ()
Education
Safety
Mobility
Air Quality (g/m3)
Healthcare
Kids' Activities
Paid Parental Leave (Days)
Family Inclusivity
Neighbourhood Safety
Family-Friendliness
Total
Latitude
Longitude

Note that all factors are scored out of 100 (unless noted otherwise above) where the higher the score, the better. Further, the score for these factors was calculated by applying a mix-max normalisation to the underlying indicator.
For further details of the scoring and methodology of the study, please refer to https://www.movinga.de/en/cities-of-opportunity-for-families.
Acknowledgements
I'd like to thank Movinga for providing the original data on their website.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43781) of an [OpenML dataset](https://www.openml.org/d/43781). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43781/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43781/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43781/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

